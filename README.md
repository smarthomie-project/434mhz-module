# 434MHz Module

## Introduction

Before installing the modules, you have to install the base first!

This is the 434MHz module for SmartHomie.

The install script (434mhz-module/install/install.sh) installs all requirements and configuration files.

## Requirements

* Any Debian based distribution (tested with Raspbian Jessie Lite)
* Internet connection
* Raspberry Pi or any other (IoT) device with Pi-like GPIO pins

## Installation

Install as follows:

```
sudo apt-get update
sudo apt-get install git-core

git clone https://gitlab.com/technikerprojekt/434mhz-module.git

cd 434mhz-module/install/
./install.sh
```

## Soldering the sensor on to the GPIO pins

| Output on sensor | GPIO pin | GPIO Description |
|:----------------:|:--------:|:----------------:|
| VCC | #02 | DC Power 5V |
| GND | #06 | Ground |
| DATA | #11 | GPIO 17 |
