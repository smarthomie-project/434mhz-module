#!/usr/bin/env bash

export LC_ALL=C
CURRENT_DIR=$(pwd)

################################################
# Copy module specific config and install
# 	required dependencies
################################################

git clone git://git.drogon.net/wiringPi $CURRENT_DIR/source/wiringPi/
git clone https://github.com/xkonni/raspberry-remote $CURRENT_DIR/source/raspberry-remote/

(cd $CURRENT_DIR/source/wiringPi && ./build)
(cd $CURRENT_DIR/source/raspberry-remote && make send)

cp $CURRENT_DIR/source/raspberry-remote/send /srv/smarthomie/scripts/send

exit 0
