#!/bin/bash

export LC_ALL=C

/srv/smarthomie/scripts/send $@

############
# check IP address
############

IP_ADDRESS=$(/bin/cat /srv/smarthomie/conf/ip_address)
CURRENT_IP_ADDRESS=$(/sbin/ifconfig | grep inet | head -n1 | sed 's/:/ /g' | awk '{ print $3 }')
MAC_ADDRESS=$(/sbin/ifconfig | /bin/grep HWaddr | /usr/bin/head -n1 | /usr/bin/awk '{ print $5 }' | /bin/sed 's/://g')
API_KEY=$(/bin/cat $currentDirectory/../conf/api_key)
BASE=$(/bin/cat $currentDirectory/../conf/base)

if [ "$IP_ADDRESS" != "$CURRENT_IP_ADDRESS" ]; then
	echo $CURRENT_IP_ADDRESS > /srv/smarthomie/conf/ip_address

	/usr/bin/curl -u $MAC_ADDRESS:$API_KEY -d "ip_address=$CURRENT_IP_ADDRESS" -X POST http://$BASE/api/update-ip-address
fi
